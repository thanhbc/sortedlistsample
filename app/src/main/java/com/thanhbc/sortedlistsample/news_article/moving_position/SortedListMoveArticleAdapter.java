package com.thanhbc.sortedlistsample.news_article.moving_position;

import android.view.ViewGroup;

import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedAdapter;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedGridItemViewHolder;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListMoveArticleAdapter extends SortedListShouldBeSortedAdapter {
    public SortedListMoveArticleAdapter(ArrayList<Article> articles) {
        super(articles);
    }

    @Override
    public SortedListMoveGridItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return SortedListMoveGridItemViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(SortedListShouldBeSortedGridItemViewHolder holder, int position) {
        ((SortedListMoveGridItemViewHolder) holder).bindTo(articleSortedList.get(position), this);
    }

    public void changeTimestamp(Article article) {
        Article changedArticle = article.dupe()
                .toBuilder()
                .publishedTime(new DateTime(article.publishedTime()).minusDays(30).getMillis())
                .build();
        int itemIndex = articleSortedList.indexOf(article);
        articleSortedList.updateItemAt(itemIndex, changedArticle);
    }
}
