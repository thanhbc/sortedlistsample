package com.thanhbc.sortedlistsample.news_article.delete_individual_item;

import android.view.ViewGroup;

import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedAdapter;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedGridItemViewHolder;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListRemoveArticleAdapter extends SortedListShouldBeSortedAdapter {
    public SortedListRemoveArticleAdapter(ArrayList<Article> articles) {
        super(articles);
    }

    @Override public SortedListShouldBeSortedGridItemViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                   int viewType) {
        return SortedListRemoveArticleGridItemViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(SortedListShouldBeSortedGridItemViewHolder holder, int position) {
        ((SortedListRemoveArticleGridItemViewHolder) holder).bindTo(articleSortedList.get(position),
                this);
    }

    public void removeArticle(Article article) {
        articleSortedList.remove(article);
    }
}
