package com.thanhbc.sortedlistsample.news_article.add_individual_item;

import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedAdapter;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListAddArticleAdapter extends SortedListShouldBeSortedAdapter {

    public SortedListAddArticleAdapter(ArrayList<Article> articles) {
        super(articles);
    }

    public void addArticle(Article article) {
        articleSortedList.add(article);
    }
}
