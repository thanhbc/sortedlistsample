package com.thanhbc.sortedlistsample.news_article.same_add_no_effect;

import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedAdapter;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListSameAddHasNoEffectAdapter extends SortedListShouldBeSortedAdapter {
    public SortedListSameAddHasNoEffectAdapter(ArrayList<Article> articles) {
        super(articles);
    }

    public void addFirstItem() {
        articleSortedList.add(articleSortedList.get(0).dupe());
    }
}
