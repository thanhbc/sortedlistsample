package com.thanhbc.sortedlistsample.news_article;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.thanhbc.sortedlistsample.R;
import com.thanhbc.sortedlistsample.news_article.batch_action.SortedListBatchOperationFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListExplorationActivity extends AppCompatActivity {
    @BindView(R.id.container)
    FrameLayout fragmentContainer;
    private Unbinder unbinder;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sortedlist_exploration);
        unbinder = ButterKnife.bind(this);
//        Fragment fragment = SortedListShouldBeSortedFragment.create();
//        Fragment fragment = SortedListSameAddHasNoEffectFragment.create();
//        Fragment fragment = SortedListAddArticleFragment.create();
//        Fragment fragment = SortedListRemoveArticleFragment.create();
//        Fragment fragment = SortedListChangeContentArticleFragment.create();
//        Fragment fragment = SortedListMoveArticleFragment.create();
        Fragment fragment = SortedListBatchOperationFragment.create();
        getFragmentManager().beginTransaction().add(fragmentContainer.getId(), fragment).commit();
    }

    @Override protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
