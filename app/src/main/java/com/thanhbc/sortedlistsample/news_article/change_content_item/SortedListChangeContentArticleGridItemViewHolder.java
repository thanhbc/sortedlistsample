package com.thanhbc.sortedlistsample.news_article.change_content_item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thanhbc.sortedlistsample.R;
import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedGridItemViewHolder;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListChangeContentArticleGridItemViewHolder extends SortedListShouldBeSortedGridItemViewHolder {
    public SortedListChangeContentArticleGridItemViewHolder(View itemView) {
        super(itemView);
    }

    public static SortedListChangeContentArticleGridItemViewHolder create(ViewGroup parent) {
        ViewGroup itemView = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grid_article, parent, false);
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.width = parent.getWidth() / 2;
        layoutParams.height = layoutParams.width;
        itemView.setLayoutParams(layoutParams);
        return new SortedListChangeContentArticleGridItemViewHolder(itemView);
    }

    public void bindTo(final Article article, final SortedListChangeContentArticleAdapter adapter) {
        super.bindTo(article);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                adapter.changeContent(article);
            }
        });
    }
}
