package com.thanhbc.sortedlistsample.news_article;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListShouldBeSortedAdapter extends RecyclerView.Adapter<SortedListShouldBeSortedGridItemViewHolder> {

    protected SortedList<Article> articleSortedList;


    public SortedListShouldBeSortedAdapter(ArrayList<Article> articles) {
        this.articleSortedList = new SortedList<>(Article.class, new SortedListAdapterCallback<Article>(this) {
            @Override
            public int compare(Article o1, Article o2) {
                return o1.compare(o2);
            }

            @Override
            public boolean areContentsTheSame(Article oldItem, Article newItem) {
                return oldItem.areContentsTheSame(newItem);
            }

            @Override
            public boolean areItemsTheSame(Article item1, Article item2) {
                return item1.areItemsTheSame(item2);
            }
        });

        articleSortedList.addAll(articles);
    }

    @Override
    public SortedListShouldBeSortedGridItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return SortedListShouldBeSortedGridItemViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(SortedListShouldBeSortedGridItemViewHolder holder, int position) {
        holder.bindTo(articleSortedList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleSortedList.size();
    }
}
