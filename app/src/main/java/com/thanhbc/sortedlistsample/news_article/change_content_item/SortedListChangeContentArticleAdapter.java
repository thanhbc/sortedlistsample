package com.thanhbc.sortedlistsample.news_article.change_content_item;

import android.view.ViewGroup;

import com.thanhbc.sortedlistsample.news_article.Article;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedAdapter;
import com.thanhbc.sortedlistsample.news_article.SortedListShouldBeSortedGridItemViewHolder;

import java.util.ArrayList;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListChangeContentArticleAdapter extends SortedListShouldBeSortedAdapter {

    public SortedListChangeContentArticleAdapter(ArrayList<Article> articles) {
        super(articles);
    }
    @Override
    public SortedListChangeContentArticleGridItemViewHolder onCreateViewHolder(ViewGroup parent,
                                                                               int viewType) {
        return SortedListChangeContentArticleGridItemViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(SortedListShouldBeSortedGridItemViewHolder holder, int position) {
        ((SortedListChangeContentArticleGridItemViewHolder) holder).bindTo(
                articleSortedList.get(position), this);
    }

    public void changeContent(Article article) {
        articleSortedList.add(article.dupe()
                .toBuilder()
                .author(Article.faker.name().firstName())
                .category(Article.CATEGORIES[2])
                .content(Article.faker.lorem().paragraph(2))
                .build());
    }
}
