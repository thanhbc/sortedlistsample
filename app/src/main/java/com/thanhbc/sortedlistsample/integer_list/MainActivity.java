package com.thanhbc.sortedlistsample.integer_list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.thanhbc.sortedlistsample.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.addButton) Button addButton;
    @BindView(R.id.removeButton) Button removeButton;

    private Unbinder mUnbinder;

    private IntegerListAdapter listAdapter;
    private ArrayList<Integer> integerAdditionList = new ArrayList<>();
    private ArrayList<Integer> integerRemovalList = new ArrayList<>();
    private Integer integerToAdd;
    private Integer integerToRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUnbinder = ButterKnife.bind(this);
        setupSeeds();
        setupRecycler();
        randomizeAddButton();
    }


    private void setupSeeds() {
        for (int i = 0; i < 5; i++) {
            integerAdditionList.add(i);
        }
    }

    private void randomizeRemoveButton() {
        integerToRemove = randIntegerToRemove();
        if (integerToRemove >= 0) {
            removeButton.setText("Remove " + integerToRemove.intValue());
        } else {
            removeButton.setText("Remove");
        }
    }

    private void randomizeAddButton() {
        integerToAdd = randIntegerToAdd();
        if (integerToAdd >= 0) {
            addButton.setText("Add " + integerToAdd.intValue());
        } else {
            addButton.setText("Add");
        }
    }

    private void setupRecycler() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listAdapter = new SortedListAdapter();
        recyclerView.setAdapter(listAdapter);
    }
    @OnClick(R.id.addButton)
    public void onAddButtonClicked() {
        if (integerToAdd >= 0) {
            listAdapter.addInteger(integerToAdd);
            integerRemovalList.add(integerToAdd);
            integerAdditionList.remove(integerToAdd);
            randomizeAddButton();
            randomizeRemoveButton();
        }
    }

    @OnClick(R.id.removeButton)
    public void onRemoveButtonClicked() {
        if (integerToRemove >= 0) {
            listAdapter.removeInteger(integerToRemove);
            integerAdditionList.add(integerToRemove);
            integerRemovalList.remove(integerToRemove);
            randomizeRemoveButton();
            randomizeAddButton();
        }
    }


    private Integer randIntegerToAdd() {
        if (integerAdditionList.size() > 0) {
            int position = (int) (Math.random() * 10) % integerAdditionList.size();
            return integerAdditionList.get(position);
        }
        return -1;
    }

    private Integer randIntegerToRemove() {
        if (integerRemovalList.size() > 0) {
            int position = (int) (Math.random() * 10) % integerRemovalList.size();
            return integerRemovalList.get(position);
        }
        return -1;
    }
    @Override protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }
}
