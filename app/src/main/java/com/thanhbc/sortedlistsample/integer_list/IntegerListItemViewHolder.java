package com.thanhbc.sortedlistsample.integer_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thanhbc.sortedlistsample.R;

/**
 * Created by thanhbc on 5/6/17.
 */

public class IntegerListItemViewHolder extends RecyclerView.ViewHolder {
    public IntegerListItemViewHolder(View itemView) {
        super(itemView);
    }

    public void bindTo(Integer integer) {
        TextView tv = (TextView) itemView;
        tv.setText(String.valueOf(integer.intValue()));
    }

    public static final IntegerListItemViewHolder create(ViewGroup parent) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_integer, parent, false);
        return new IntegerListItemViewHolder(view);
    }
}
