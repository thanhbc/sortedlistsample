package com.thanhbc.sortedlistsample.integer_list;

import android.support.v7.widget.RecyclerView;

/**
 * Created by thanhbc on 5/6/17.
 */

public abstract class IntegerListAdapter extends RecyclerView.Adapter<IntegerListItemViewHolder> {
    protected abstract void addInteger(Integer integer);

    protected abstract void removeInteger(Integer integer);
}
