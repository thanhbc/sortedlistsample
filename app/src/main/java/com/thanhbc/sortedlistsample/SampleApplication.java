package com.thanhbc.sortedlistsample;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by thanhbc on 5/7/17.
 */

public class SampleApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
