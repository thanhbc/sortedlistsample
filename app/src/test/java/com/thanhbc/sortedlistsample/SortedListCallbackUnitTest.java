package com.thanhbc.sortedlistsample;

import android.support.v4.util.Pair;
import android.support.v7.util.SortedList;

import com.thanhbc.sortedlistsample.news_article.Article;

import org.junit.Rule;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Created by thanhbc on 5/7/17.
 */

public class SortedListCallbackUnitTest {

    @Rule
    public SortedListTestSetup fixture = new SortedListTestSetup();

    private void addArticles() {
        assertThat(fixture.sortedList().size()).isEqualTo(0);
        for (Article article : fixture.shuffledArticles()) {
            fixture.sortedList().add(article.dupe());
        }
        assertThat(fixture.sortedList().size()).isEqualTo(fixture.orderedArticleList().size());
    }

    @Test
    public void testAddShouldSortListIrrespectiveOfOrder() throws Exception {
        //SortedList should be sorted.
        addArticles();
        for (int i = 0; i < fixture.orderedArticleList().size(); i++) {
            assertThat(fixture.sortedList().get(i)).isEqualTo(fixture.orderedArticleList().get(i));
        }
    }

    @Test
    public void testAdditionOfSameObjectShouldNotChangeSortedList() throws Exception {
        //Adding multiple same objects into sorted list should not have any effect
        assertThat(fixture.sortedList().size()).isEqualTo(0);
        Article article = fixture.orderedArticleList()
                .get(Article.Utils.randomWithRange(0, fixture.orderedArticleList().size() - 1));

        //add 1 article
        fixture.sortedList().add(article);

        //check recorder insert operation is triggered
        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(1);

        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(0);

        //clear recorder
        fixture.callbackRecorder().clear();

        //sorted list still have 1st added item
        assertThat(fixture.sortedList().size()).isEqualTo(1);

        //add duplicate article 10 times
        for (int i = 0; i < Article.Utils.randomWithRange(1, 10); i++) {
            fixture.sortedList().add(article.dupe());
        }

        //list size not changed
        assertThat(fixture.sortedList().size()).isEqualTo(1);


        //no operation is triggered
        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(0);
    }

    @Test
    public void testInsertions() throws Exception {
        assertThat(fixture.sortedList().size()).isEqualTo(0);

        for (int i = 0; i < fixture.shuffledArticles().size(); i++) {
            Article articleToAdd = fixture.shuffledArticles().get(i).dupe();
            int positionTobeInserted = -1;
            for (int j = 0; j < fixture.sortedList().size(); j++) {
                Article jth = fixture.sortedList().get(j);
                if (jth.compare(articleToAdd) >= 0) {
                    positionTobeInserted = j;
                    break;
                }
            }
            if (positionTobeInserted == -1) {
                positionTobeInserted = fixture.sortedList().size();
            }

            fixture.sortedList().add(articleToAdd);
            assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(i + 1);
            Pair<Integer, Integer> insertionPair = fixture.callbackRecorder().insertions().get(i);
            assertThat(insertionPair.first).isEqualTo(positionTobeInserted);
        }

        //no other operation is triggered
        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(0);
    }

    @Test
    public void testChanges() throws Exception {
        assertThat(fixture.sortedList().size()).isEqualTo(0);
        addArticles();
        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(
                fixture.shuffledArticles().size());

        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(0);

        fixture.callbackRecorder().clear();

        int index = Article.Utils.randomWithRange(0, fixture.sortedList().size() - 1);
        Article article = fixture.sortedList().get(index);
        Article contentChanged =
                article.toBuilder().content(Article.Utils.shuffleString(article.content())).build();
        fixture.sortedList().add(contentChanged);
        Article changed = fixture.sortedList().get(index);
        assertThat(changed.content()).isEqualTo(contentChanged.content());
        assertThat(changed.content()).isNotEqualTo(article.content());


        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);

        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(1);
        assertThat(fixture.callbackRecorder().changes().get(0).first).isEqualTo(index);
        assertThat(fixture.callbackRecorder().changes().get(0).second).isEqualTo(1);
    }

    @Test
    public void testDeletions() throws Exception {
        assertThat(fixture.sortedList().size()).isEqualTo(0);
        addArticles();

        fixture.callbackRecorder().clear();

        int index = Article.Utils.randomWithRange(0, fixture.sortedList().size() - 1);
        Article articleToDelete = fixture.sortedList().get(index).dupe();

        boolean deleted = fixture.sortedList().remove(articleToDelete);
        assertThat(deleted).isTrue();

        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().changes().size()).isEqualTo(0);

        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(1);

        assertThat(fixture.callbackRecorder().deletions().get(0).first).isEqualTo(index);
        assertThat(fixture.callbackRecorder().deletions().get(0).second).isEqualTo(1);

    }

    @Test
    public void testMoves() throws Exception {
        assertThat(fixture.sortedList().size()).isEqualTo(0);
        addArticles();

        fixture.callbackRecorder().clear();

        int index1 = 2;
        Article articleOne = fixture.sortedList().get(index1);
        assertThat(fixture.sortedList().indexOf(articleOne)).isEqualTo(index1);

        fixture.sortedList().updateItemAt(index1, articleOne.toBuilder().publishedTime(1).build());

        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(0);
        assertThat(fixture.callbackRecorder().deletions().size()).isEqualTo(0);

        //check only 1 move operation is performed
        assertThat(fixture.callbackRecorder().moves().size()).isEqualTo(1);


        //check moves operation is from pos 2 to pos 0
        assertThat(fixture.callbackRecorder().moves().get(0).first).isEqualTo(2);
        assertThat(fixture.callbackRecorder().moves().get(0).second).isEqualTo(0);

        //check change operation is 2
        assertThat(fixture.callbackRecorder().changes().get(0).first).isEqualTo(2);
    }

    @Test
    public void testBatchedCallbacks() throws Exception {
        SortedList<Article> articleSortedList = new SortedList<>(Article.class,
                new SortedList.BatchedCallback<>(fixture.callbackRecorder()));
        articleSortedList.beginBatchedUpdates();
        //insert all articles
        for (int i = 0; i < fixture.shuffledArticles().size(); i++) {
            Article articleToAdd = fixture.shuffledArticles().get(i).dupe();
            articleSortedList.add(articleToAdd);
            //validate no operation is triggered before endBatchedUpdates called
            assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(0);
        }
        articleSortedList.endBatchedUpdates();

        //only 1 insertions operation is triggered
        assertThat(fixture.callbackRecorder().insertions().size()).isEqualTo(1);
        assertThat(fixture.callbackRecorder().insertions().get(0).first).isEqualTo(0);
        assertThat(fixture.callbackRecorder().insertions().get(0).second).isEqualTo(
                fixture.shuffledArticles().size());
    }
}
